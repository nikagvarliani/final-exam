package module

data class Product(
    val description : String = "",
    val price : String = "",
    val imageURL : String = ""
)
